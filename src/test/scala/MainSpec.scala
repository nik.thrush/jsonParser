import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.util.{Failure, Success}

class MainSpec extends AnyFlatSpec with Matchers with GivenWhenThen{

  "JsonParser.getDataFromFile function" should "return Try[Seq[Cat]] created after file parsing" in {
    Given("JsonParser and path to input file with one set of Cat parameters")
    val path = "test.json"
    val jsonParser = JsonParser(path)

    When("getDataFromFile function is called for JsonParser")
    val res = jsonParser.getDataFromFile

    Then("result should be with Success type and contains List with one Cat object")
    res match {
      case Success(value) => value should equal(List(Cat("Test","test","333")))
      case Failure(exception) => fail(s"Test has been failed due to unexpected error: $exception")
    }
  }

}
