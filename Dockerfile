FROM centos:centos8

LABEL maintainer="nikodrozd"

RUN yum install -y epel-release
RUN yum update -y && yum install -y wget

# INSTALL JAVA
RUN yum install -y java-11-openjdk

# INSTALL COURSIER
RUN curl -fLo cs https://git.io/coursier-cli-"$(uname | tr LD ld)"
RUN chmod +x cs
RUN ./cs install cs
RUN rm cs

ENV PATH="$PATH:/root/.local/share/coursier/bin"

#SETUP SCALA/SBT
RUN cs setup

#SETUP GIT
RUN dnf install git -y

WORKDIR /root
EXPOSE 8080